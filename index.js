
//number constants
const elBankBalance = document.getElementById("currentBalance");
const elLoanBalance = document.getElementById("currentLoan");
const elCurrentPay = document.getElementById("workBalance");

//button constants
const loanBtn = document.getElementById("btn-takeLoan");
const workBtn = document.getElementById("btn-work");
const bankBtn = document.getElementById("btn-bank");
const repayBtn = document.getElementById("btn-repayLoan");
const buyBtn = document.getElementById("btn-buy");

//laptop constants
const selectLaptop = document.getElementById("selectLaptop");
const laptopTitle = document.getElementById("featureTitle");
const featuresInfo = document.getElementById("features");
const laptopPrice = document.getElementById("laptopPrice");

//variables
let currentBalance = 0;
let currentLoan = 0;
let currentPay = 0;
let loanTaken = false;
let currentSelectedLaptop;

//arrays
let laptops = [];
let laptopSepcs = [];

function applyForLoan(){
    checkThatInputIsValid(prompt("Enter loan amount", ""));
}

function checkThatInputIsValid(input){
    if(isNaN(+input)) alert("Not a number");
    else if(+input > currentBalance*2) alert("You cannot take a loan that's twise the size of bank balance");
    else if(+input <= 0) alert("unvalid value");
    else if(loanTaken) alert("Can't take a new loan before privious loan is repaid");
    else {
        loanTaken = true;
        currentBalance += +input;
        currentLoan += +input;
        handelInnerText();
    }
}

//handel all textfeilds
function handelInnerText(){
    //set text fields to 
    elBankBalance.innerText = "Balance: " + +currentBalance;
    elCurrentPay.innerText = "Pay: " + currentPay
    
    //check if active loan
    if(loanTaken){
        elLoanBalance.innerText = "Outstanding loan: " + currentLoan;
        elLoanBalance.style.display = "block";
        repayBtn.style.display = "block"
    }
    else {
        //if no active loan, don't show loan number og repay button
        elLoanBalance.style.display = "none";
        repayBtn.style.display = "none";
    }
}

//handel work button
function working(){
    currentPay += 100;
    handelInnerText();
}

//handel repay button
function handelRepay(){
    //handel repay amount by choosing the lowest value of current pay amount and current loan
    let repayAmount = Math.min(+currentLoan, +currentPay)
    currentLoan -= repayAmount;
    currentPay -= repayAmount;
    if(currentLoan === 0) loanTaken = false;
    handelInnerText();
}

//handel bank button
function handelBank(){

    if(!currentLoan){
        currentBalance += +currentPay;
        currentPay = 0;
        handelInnerText();
        return;
    }

    //take 10% and subtract it from total to avoid rounding error
    let transferLoan = +currentPay * 0.1
    let transferBank = +currentPay - +transferLoan;

    currentPay = 0;
    currentLoan -= +transferLoan;
    currentBalance += +transferBank;
    
    if(!transferBank + transferLoan == currentPay) alert("Something is wrong)");

    handelInnerText();
}
 
//fetch information and proces it to json format and sends it to a handler
function fetchLaptops(){
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToSelectMenu(laptops))
}

//process data, and set default values
function addLaptopsToSelectMenu(laptops){

    //process the information
    laptops.forEach(laptop => addLaptopToMenu(laptop));
    
    //set default value
    selectedLaptop = laptops[0];

    //access the values to present in server
    laptopTitle.innerText = laptops[0].title;
    laptopInfo.innerText = laptops[0].description;
    laptopPrice.innerText = laptops[0].price + " kr";

    setLaptopSpecs(laptops[0].specs);
}

//process data (element in array), so that we can access it when changes is done it the select field in index.html
//bind the values to a laptop element so we can access values later
function addLaptopToMenu(laptop){
    
    const laptopElement = document.createElement("option");
    
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    selectLaptop.appendChild(laptopElement);
}

//access different specs for different laptops
function setLaptopSpecs(specs) {
    console.log("setLaptopSpecs for current laptop: ")
    
    //make fields empty to avoid stacking values
    featuresInfo.innerText = "";
    featuresInfo.innerHTML = "";
    
    for (spec in specs) {
        const specElement = specs[spec];
        console.log(specElement)

        featuresInfo.innerText += specElement;
        featuresInfo.innerHTML += "<br/>";
    }
}

//Disclaimer: this is not mine, had to get help with this one to make it work
const handleLaptopMenuChange = e => {
    
    //access selected laptop
    selectedLaptop = laptops[e.target.selectedIndex];

    //present connected values
    laptopTitle.innerText = selectedLaptop.title;
    laptopInfo.innerText = selectedLaptop.description;
    laptopPrice.innerText = selectedLaptop.price + " kr";
    
    laptopSpecs = selectedLaptop.specs;
    
    setLaptopSpecs(selectedLaptop.specs);
}

//handel buy button
function buyLaptop(){
    if(currentBalance < +selectedLaptop.price) alert(`You don't have the founds to buy the ${selectedLaptop.title}`);
    alert(`You just bought the ${selectedLaptop.title}`);
    currentBalance -= selectedLaptop.price;
    handelInnerText();
}

//default values
elLoanBalance.style.display = "none";
repayBtn.style.display = "none";
elBankBalance.innerText = "Balance: " + currentBalance;

//fetch infomation
fetchLaptops();

//eventlistners for buttons and fields
loanBtn.addEventListener("click", applyForLoan);
workBtn.addEventListener("click", working);
repayBtn.addEventListener("click", handelRepay);
bankBtn.addEventListener("click", handelBank);
buyBtn.addEventListener("click", buyLaptop);
selectLaptop.addEventListener("change", handleLaptopMenuChange);